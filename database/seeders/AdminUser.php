<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\State;
use Illuminate\Support\Facades\Hash;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
        	'name' => 'admin',
        	'email' => 'admin@hotmail.com',
        	'password' => Hash::make('admin'),
        	'admin' => true
        ]);
        $user = User::create([
            'name' => 'Carlos',
            'email' => 'usuario@gmail.com',
            'password' => Hash::make('usuario'),
            'admin' => false
        ]);
        $category = Category::create([
            'name' => 'Acción',
            'description' => 'Peliculas de accion'
        ]);
        $categoryB = Category::create([
            'name' => 'Terror',
            'description' => 'Peliculas de terror'
        ]);
        $stateD = State::create(['name' => 'Disponible']);
        $stateC = State::create(['name' => 'Comprado']);
        $stateA = State::create(['name' => 'Alquilado']);
    }
}
