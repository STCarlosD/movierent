<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\TranslationController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Ruta home para admin
Route::get('/home', [HomeController::class, 'index'])->name('home');

//Ruta home para usuario
Route::get('/user', [HomeController::class, 'getUser'])->name('user-home');

//Grupo de rutas para proyecto
Route::middleware(['auth', 'verifyAdmin'])->group(function () {
    Route::resource('categories', CategoryController::class);
    Route::resource('movies', MovieController::class);
    Route::resource('transactions', TransactionController::class); 
    //Rutas para reportes
    Route::get('/reports/users-report', [ReportController::class, 'getUsersData'])->name('report.users-report');
    Route::get('/reports/users-report/download-pdf', [ReportController::class, 'downloadUserReportPdf'])->name('report.users.download-pdf');   
    Route::get('/reports/users-report/download-excel', [ReportController::class, 'downloadUserReportExcel'])->name('report.users.download-excel');   
});

//Ruta para los detalles de la película
Route::get('/user-show/{id}', [MovieController::class, 'userShow'])->name('movies.user-show');
//Ruta para comprar pelicula
Route::post('/transactions/buy/{id}', [TransactionController::class, 'buy'])->name('transactions.buy');
//Ruta para rentar pelicula
Route::post('/transactions/rent/{id}', [TransactionController::class, 'rent'])->name('transactions.rent');
//Ruta para listar peliculas rentadas 
Route::get('/moviest/rented', [MovieController::class, 'rentedMovies'])->name('movies.rented');
//Ruta para deolver peliculas
Route::post('/movies/return/{id}', [MovieController::class, 'returnMovie'])->name('movies.return-movie');



