<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $fillable = [
		'name', 
		'picture', 
		'description',
		'year',
		'price',
		'rent_price',
		'category_id',
		'state_id'
	];

	public function state(){
		return $this->hasOne(State::class);
	}
	public function category(){
		return $this->hasOne(Category::class);
	}
	public function transactions(){
		return $this->hasMany(Transaction::class);
	}
}
