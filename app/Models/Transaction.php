<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
		'type', 
		'start_date',
		'end_date',
		'return_date',
		'buy_date',
		'debt',
		'open',
		'movie_id',
		'user_id'
	];

	public function movie(){
		return $this->belongsTo(Movie::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}
}
