<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
		'name', 'description'
	];

	public function my_update($request){       
        self::update($request->all());
    }

    public function movies(){
		return $this->hasMany(Movie::class);
	}
}
