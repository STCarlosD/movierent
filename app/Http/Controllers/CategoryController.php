<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorno a vista junto a las categorías
        return view('category.index', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retorno a vista con formulario para registrar
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validando la request
        $request->validate([            
            'name' => 'required',
            'description' => 'required',                       
        ],
        [//colocando mensajes de error en cada caso          
            'name.required' => 'El nombre es requerido',
            'description.required' => 'La descripción es requerida.'                
        ]);
        //creando la categía y redireccionando con 'created' para indicar que se creó bien
        Category::create($request->all());
        return redirect()->route('categories.index')->with('created', 'ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //retornando a vista para editar formulario, enviando la categoría a editar
        return view('category.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //validando la request
        $request->validate([            
            'name' => 'required',
            'description' => 'required',                       
        ],
        [//colocando mensajes de error en cada caso          
            'name.required' => 'El nombre es requerido',
            'description.required' => 'La descripción es requerida.'                
        ]);
        //se utiliza funcion update creada en modelo
        $category->my_update($request);
        return redirect()->route('categories.index')->with('edited', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //se elimina la categoría y se retorna al index junto con "deleted" para indicar que se elimino
        $category->delete();
        return redirect()->route('categories.index')->with('deleted', 'ok');
    }
}
