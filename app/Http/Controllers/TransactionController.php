<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function buy(Request $request, $id)
    {
        setlocale(LC_ALL,"es_ES");

        $today = getdate();
        $buyDate = $today["year"] . "-" . $today["mon"] . "-" . $today["mday"];        
        $movie = Movie::find($id);
        $transaction = Transaction::create([
            'type' => "C",
            'debt' => $movie->price,
            'buy_date' => $buyDate,
            'user_id' => $request['user_id'],
            'movie_id' => $id,
            'open' => false
        ]);

        return redirect()->route('user-home')->with('buyed', 'ok');
    }

    public function rent(Request $request,$id)
    {
        setlocale(LC_ALL,"es_ES");

        $today = getdate();
        $startDate = $today["year"] . "-" . $today["mon"] . "-" . $today["mday"];        
        $movie = Movie::find($id);
        $transaction = Transaction::create([
            'type' => "A",
            'debt' => $movie->rent_price,     
            'start_date' => $startDate,       
            'user_id' => $request['user_id'],
            'movie_id' => $id,
            'open' => true
        ]);

        return redirect()->route('user-home')->with('rented', 'ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
