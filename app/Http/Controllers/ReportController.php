<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class ReportController extends Controller
{
    public function getUsersData(){
    	$users = User::all();
    	return view('report.users-report', [
    		'users' => $users
    	]);
    }

    public function downloadUserReportPdf(){
    	$users = User::all();
    	$pdf = PDF::loadView('report.users-report', compact('users'));

    	return $pdf->download('usuarios.pdf');
    }

    public function downloadUserReportExcel(){
    	return Excel::download(new UsersExport, 'usuarios.xlsx');
    }
}
