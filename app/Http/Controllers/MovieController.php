<?php

namespace App\Http\Controllers;

use Storage;
use App\Models\Movie;
use App\Models\Category;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //retorno a vista junto a las peliculas
        return view('movie.index', [
            'movies' => Movie::all(),
            'categories' => Category::all()
        ]);
    }


    public function rentedMovies()
    {
        $movies = DB::select("select m.id, t.id as 'tranId',m.name, m.year, t.debt from movies m inner join transactions t 
            on t.movie_id = m.id
            where t.type = 'A' and t.open = 1 and t.user_id = ?", [Auth::user()->id]);

        return view('movie.rented-index', [
            'movies' => $movies
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //retorno a vista con formulario para registrar
        return view('movie.create', [
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validando la request
        $request->validate([            
            'name' => 'required',
            'description' => 'required',   
            'picture' => 'required',
            'year' => 'required', 
            'price' => 'required',
            'category_id' => 'required',            
        ],
        [//colocando mensajes de error en cada caso          
            'name.required' => 'El nombre es requerido',
            'description.required' => 'La descripción es requerida.',
            'picture.required' => 'La portada es requerida.',
            'year.required' => 'El año es requerido.',
            'price.required' => 'El precio es requerido.',
            'category_id.required' => 'La categoría es requerida.'
        ]);

        $file = $request->file('picture');
        $extension = $file->getClientOriginalExtension();
        $nombre=$file->getClientOriginalName();
        Storage::disk('public')->put($nombre,  \File::get($file));
        $pathToFile = $nombre;

        $movie = Movie::create([
            'name' => $request['name'],
            'year' => $request['year'],
            'description' => $request['description'],
            'picture' => $pathToFile,
            'category_id' => $request['category_id'],
            'price' => $request['price'],
            'rent_price' => ($request['price'] * 0.25),
            'state_id' => 1            
        ]);
        return redirect()->route('movies.index')->with('created', 'ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('movie.show', [
            'movie' => $movie,
            'categories' => Category::all()
        ]);
    }

    public function userShow($id)
    {
        $movie = Movie::find($id);
        return view('movie.user-show', [
            'movie' => $movie,
            'categories' => Category::all()
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //retornando a vista para editar formulario, enviando la pelicula a editar
        return view('movie.edit', [
            'movie' => $movie,
            'categories' => Category::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validando la request
        $request->validate([            
            'name' => 'required',
            'description' => 'required',   
            'year' => 'required', 
            'price' => 'required',
            'category_id' => 'required',            
        ],
        [//colocando mensajes de error en cada caso          
            'name.required' => 'El nombre es requerido',
            'description.required' => 'La descripción es requerida.',
            'year.required' => 'El año es requerido.',
            'price.required' => 'El precio es requerido.',
            'category_id.required' => 'La categoría es requerida.'
        ]);
        //verifica si se cambio la imagen de portada
        if($request->hasFile('picture') ){
            $file = $request->file('picture');
            $extension = $file->getClientOriginalExtension();
            $nombre=$file->getClientOriginalName();
            Storage::disk('public')->put($nombre,  \File::get($file));
            $pathToFile = $nombre;
            //se sube la nueva imagen y se cambia en la bdd
            $movie = Movie::find($id);
            $movie->name = $request['name'];
            $movie->description = $request['description'];
            $movie->year = $request['year'];
            $movie->price = $request['price'];
            $movie->rent_price = ($request['price'] * 0.25);
            $movie->category_id = $request['category_id'];
            $movie->picture = $pathToFile;
            $movie->save();
        }else{
            //se realiza un update normal sin cambiar la imagen de portada
            $movie = Movie::find($id);
            $movie->name = $request['name'];
            $movie->description = $request['description'];
            $movie->year = $request['year'];
            $movie->price = $request['price'];
            $movie->rent_price = ($request['price'] * 0.25);
            $movie->category_id = $request['category_id'];
            $movie->picture = $request['pathToFile'];
            $movie->save();
        }        
        return redirect()->route('movies.index')->with('edited', 'ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //se elimina la pelicula y se retorna al index junto con "deleted" para indicar que se elimino
        $movie->delete();
        return redirect()->route('movies.index')->with('deleted', 'ok');
    }

    public function returnMovie(Request $request, $id)
    {
        //Crear transaccion de tipo Retorno
        $returnTransaction = Transaction::create([
            'type' => 'R',
            'user_id' => $request['user_id'],
            'movie_id' => $id,
            'open' => false,
            'debt' => $request['debt']
        ]);
        //se cambia la transaccion de alquilada a cerrado (open = false)
        $rentedTransaction = Transaction::find($request['transactionId']);
        $rentedTransaction->open = false;
        $rentedTransaction->save();
        return redirect()->route('movies.rented')->with('returned', 'ok');
    }
}
