<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Reporte de usuarios</title>

	<style>
		#table{
			font-family: Arial;
			border-collapse: collapse;
			width: 100;
		}
		#table td , #table th{
			border: 1px solid #ddd;
			padding: 8px;
		}
		#table th{
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #F4C450;
			color: #fff;
		}
	</style>
</head>
<body>

	<h1>Reporte usuarios registrados en Movie Rent.</h1>
	<table id="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Tipo de usuario</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
                <tr>
                	<td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->admin ? 'Administrador' : 'Normal' }}</td>
               </tr>
           @endforeach
		</tbody>
	</table>
</body>
</html>