@extends('layouts.app')

@section('content')
<center><h1>Catálogo</h1></center>
<div class="container">
    <div class="card-deck mt-4">
        <div class="row">
            @foreach($movies as $movie)
                
                <div class="col-md-4 mb-4">
                    <div class="card text-center border-info">
                        <div class="card-body">
                            <h4 class="card-title">{{$movie->name}}</h4>
                            <img class="img-responsive img-rounded" src="{{ asset('storage/'.$movie->picture) }}" style = "max-height: 500px; max-width: 200px;">
                            <p class="card-text">{{$movie->description}}</p>
                            <a href="{{ route('movies.user-show', $movie->id) }}" class="btn btn-primary">Más información</a>
                        </div>
                    </div>
                </div>            
                
            @endforeach
        </div>
    </div>
</div>
@endsection

@section('js')

@if (Session::has('buyed'))
        <script>
            toastr.success("Película comprada.");
        </script>
@endif

@if (Session::has('rented'))
        <script>
            toastr.success("Película rentada.");
        </script>
@endif

@endsection