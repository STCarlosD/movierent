@extends('layouts.app')
@section('css')
    <!-- CSS DATA TABLE -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="ml-1">
                <h2>Categorías</h2>
                <p></p>
            </div>
            <div class="pull-left">
                <a href="{{ route('categories.create') }}" class="btn btn-outline-success">
                    {{ __('Agregar categoría') }}
                </a>
            </div>               
            <br>
            <div class="card">
                <div class="card-body">
                    <table class="table table-stripped table-responsive  tex-center" style="width: 100%"
                    id="clients">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($categories as $item)
                        <tr id="sid{{ $item->id }}">
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>                            
                            <td>            
                            <div class="row">
                                <div class="col">
                                    <a href="{{ route('categories.edit', $item) }}" class="btn" style="background-color: #FDC02E" title="Editar categoría"><i class="fa fa-pencil" aria-hidden="true">Editar</i>
                                    </a>     
                                </div>

                                <div class="col">
                                    <form action="{{ route('categories.destroy', $item) }}" method="post">
                                            {{csrf_field()}}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger" type="submit">Eliminar</button>
                                    </form>
                                </div>
                            </div>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


</div>

@endsection

@section('js')
    {{-- JS DATATABLE --}}
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/responsive.bootstrap4.min.js" defer></script>
    <script>
        $(document).ready(function() {
            $('#clients').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });
        });

    

    </script>
    @if (Session::has('created'))
        <script>
            toastr.success("Categoría creada.");
        </script>
    @endif
    @if (Session::has('deleted'))
        <script>
            toastr.error("Categoría eliminada.");
        </script>
    @endif
    @if (Session::has('edited'))
        <script>
            toastr.success("Categoría editada.");
        </script>
    @endif
@endsection
