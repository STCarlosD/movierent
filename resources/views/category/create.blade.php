@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="{{ route('categories.store') }}" method="POST">
            @csrf
            <div class="card">
                <div class="card-header">
                    <h2>Crear categoría</h2>
                </div>
                <div class="card-body">          
                    <div class="form-row">
                        <div class="form-group col-md-12">

                            <label for="name">Nombre</label>
                            <span class="fa fa-building   form-control-feedback"></span>
                            <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ old('name') }}">                                                       
                            @error('name')
                                <span class=”invalid-feedback” role=”alert”>
                                    <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror

                            <label for="description">Descripción</label>
                            <span class="fa fa-building   form-control-feedback"></span>
                            <textarea class="form-control" name="description" cols="4" id="description"></textarea>                                                     
                            @error('description')
                                <span class=”invalid-feedback” role=”alert”>
                                    <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                    <strong class="text-danger">{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>                    
                    <button type="submit" class="btn btn-primary">Registrar</button>
                </div>
            </div>
        </form>
    </div>
@endsection
