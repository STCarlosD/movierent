@extends('layouts.app')
@section('css')
    <!-- CSS DATA TABLE -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="ml-1">
                <h2>Películas</h2>
                <p>Lista de las películas pendientes de regresar</p>
            </div>           
            <br>
            <div class="card">
                <div class="card-body">
                    <table class="table table-stripped table-responsive  tex-center" style="width: 100%"
                    id="movies">                                                
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Año</th>
                            <th>Deuda</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($movies as $item)
                        <tr>
                            <td>{{ $item->name }}</td>  
                            <td>{{ $item->year }}</td>                                                  
                            <td>{{ $item->debt }}</td>
                            <td>            
                                <form action="{{ route('movies.return-movie', $item->id) }}" method="post">
                                    {{csrf_field()}}                                    
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="debt" value="{{$item->debt }}">
                                    <input type="hidden" name="transactionId" value="{{$item->tranId }}">
                                    <button class="btn btn-primary" type="submit">Devolver</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


</div>

@endsection

@section('js')
    {{-- JS DATATABLE --}}
    
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js" defer></script>
    
    <script>
        $(document).ready(function() {
            var table = $('#movies').DataTable({
                "search": {regex: true},
                "order": [[ 1, "asc" ]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });

        });

    </script>
    @if (Session::has('returned'))
        <script>
            toastr.success("Película retornada.");
        </script>
    @endif
@endsection
