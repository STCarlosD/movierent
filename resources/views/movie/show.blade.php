@extends('layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <form>
            @csrf
            <div class="card">
                <div class="card-header">
                    <h2>Detalles de película : {{$movie->name}}</h2>
                </div>
                <div class="card-body">         
                    <div class="form-row justify-content-center align-items-center">
                        <div class="">

                            <div class="form-outline mb-4">
                                <label for="picture">Portada</label><br>
                                <span class="fa fa-building   form-control-feedback"></span>                                                        
                                <img class="img-responsive img-rounded" src="{{ asset('storage/'.$movie->picture) }}" style = "max-height: 500px; max-width: 200px;">
                            </div>

                            <div class="form-outline mb-4">
                                <label for="name">Nombre</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <input type="text" class="form-control" readonly name="name" id="name" placeholder="" value="{{ $movie->name }}">
                            </div>

                            <div class="form-outline mb-4">
                                <label for="description">Sinopsis</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <textarea class="form-control" name="description" readonly cols="4" id="description">{{$movie->description}}</textarea>
                            </div>

                            <div class="form-outline mb-4">
                                <label for="year">Año de estreno</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <input type="number" class="form-control" readonly name="year" id="datepicker" value="{{$movie->year}}" />
                            </div>

                            <div class="form-outline mb-4">
                                <label for="price">Precio</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <div class="input-group-addon">USD</div>
                                <input type="number" name="price" min="0.00" readonly step="0.05" value="{{$movie->price}}" class="form-control" placeholder="$">
                            </div>  

                            <div class="form-outline mb-4">
                                <div class="form-group">
                                    <label for="category">Categoría</label>
                                    @php
                                        foreach ($categories as $category)
                                            $movie->category_id == $category->id ?  : $categoryName = $category->name
                                    @endphp
                                    <input type="text" class="form-control" readonly name="category" id="name" placeholder="" value="{{$categoryName}}">                                        
                                </div>
                            </div>  

                            
                        </div>
                    </div>              
                </div>
            </div>
        </form>
    </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
        autoclose:true
    });
</script>
@endsection
