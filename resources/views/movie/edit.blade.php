@extends('layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        <form action="{{ route('movies.update', $movie->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            {{ method_field('PUT') }}
            <div class="card">
                <div class="card-header">
                    <h2>Editando película : {{$movie->name}}</h2>
                </div>
                <div class="card-body">         
                    <div class="form-row justify-content-center align-items-center">
                        <div class="">
                            <div class="form-outline mb-4">
                                <label for="name">Nombre</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <input type="text" class="form-control" name="name" id="name" placeholder="" value="{{ $movie->name }}">                                                       
                                @error('name')
                                    <span class=”invalid-feedback” role=”alert”>
                                        <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label for="description">Sinopsis</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <textarea class="form-control" name="description" cols="4" id="description">{{$movie->description}}</textarea>                                                     
                                @error('description')
                                    <span class=”invalid-feedback” role=”alert”>
                                        <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <input type="hidden" name="pathToFile" value="{{$movie->picture}}">
                                <label for="picture">Portada</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <div class="file-field input-field">
                                    <div class="form-group">                                        
                                        <input type="file" name="picture" class="form-control-file" id="picture">
                                    </div>

                                    @error('picture')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-outline mb-4">
                                <label for="year">Año de estreno</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <input type="number" class="form-control" name="year" id="datepicker" value="{{$movie->year}}" />                                                     
                                @error('year')
                                    <span class=”invalid-feedback” role=”alert”>
                                        <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-outline mb-4">
                                <label for="price">Precio</label>
                                <span class="fa fa-building   form-control-feedback"></span>
                                <div class="input-group-addon">USD</div>
                                <input type="number" name="price" min="0.00" step="0.05" value="{{$movie->price}}" class="form-control" placeholder="$">
                                 
                                @error('price')
                                    <span class=”invalid-feedback” role=”alert”>
                                        <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  

                            <div class="form-outline mb-4">
                                <div class="form-group">
                                    <label for="category">Categoría</label>
                                    <select class="form-control" id="category_id" name="category_id">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}"
                                                {{ $movie->category_id == $category->id ? 'selected' : '' }}>
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                   </select>
                                </div>
                                 
                                @error('category_id')
                                    <span class=”invalid-feedback” role=”alert”>
                                        <i class="fa fa-exclamation" aria-hidden="true" class="text-danger"></i>
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  

                            
                        </div>
                    </div>    
                    <div class="form-row justify-content-center align-items-center">
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>                 
                </div>
            </div>
        </form>
    </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
        autoclose:true
    });
</script>
@endsection
