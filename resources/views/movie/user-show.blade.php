@extends('layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class="container">
        

        <div class="card-deck">
            <div class="card">
                <div class="card-body text-center">
                    <img class="img-responsive img-rounded" src="{{ asset('storage/'.$movie->picture) }}" style = "max-height: 900px; max-width: 400px;">
                </div>

                <div class="row text-center">
                    <div class="col">
                        <form action="{{ route('transactions.buy', $movie->id) }}" method="POST" name="frmComprar">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <button type="submit" name="frmComprar" class="btn btn-primary">Comprar</button>
                        </form>     
                    </div> 
                    <div class="col">
                        <form action="{{ route('transactions.rent', $movie->id) }}" method="POST" name="frmRentar">
                            @csrf
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <button type="submit" name="frmRentar" class="btn btn-primary">Rentar</button>
                        </form>  
                    </div> 
                </div>
                <br>
            </div>

                <div class="card">
                    <div class="card-body text-center">
                        <h1>{{$movie->name}}</h1>
                        <br>

                        <h4>Año de estreno: {{$movie->year}}</h4><br>
                        <h4>Sinopsis: {{$movie->description}}</h4><br>                        
                        <h4>Precio de compra: $ {{$movie->price}}</h4><br>
                        <h4>Precio de renta (5 días): $ {{$movie->rent_price}}</h4><br>
                    </div>
                </div>         
            </div>
        
    </div>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    $("#datepicker").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years",
        autoclose:true
    });
</script>
@endsection
