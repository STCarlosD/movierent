@extends('layouts.app')
@section('css')
    <!-- CSS DATA TABLE -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="ml-1">
                <h2>Películas</h2>
                <p>Lista de las películas registradas</p>
            </div>
            <div class="pull-left">
                <ul>
                    <a href="{{ route('movies.create') }}" class="btn btn-outline-success">
                        {{ __('Agregar película') }}
                    </a>   
                    <button class="btn btn-primary" id="dispo">Disponible</button>
                    <button class="btn btn-primary" id="indispo">Indisponible</button>
                </ul>
            </div>               
            <br>
            <div class="card">
                <div class="card-body">
                    <table class="table table-stripped table-responsive  tex-center" style="width: 100%"
                    id="movies">                                                
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Año</th>
                            <th>Disponibilidad</th>
                            <th>Categoría</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    @foreach ($movies as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>  
                            <td>{{ $item->year }}</td>                          
                            @if ($item->state_id == 2)                                 
                                <td>Comprada</td>
                            @elseif($item->state_id == 3)
                                <td>Alquilada</td>
                            @else
                                <td>Disponible</td> 
                            @endif

                            <td>
                                @foreach ($categories as $category)
                                    {{ $item->category_id == $category->id ? $category->name : '' }}

                                                
                                @endforeach
                            </td>   
                            <td>            
                                <form action="{{ route('movies.destroy', $item) }}" method="post">
                                    {{csrf_field()}}
                                    {{ method_field('DELETE') }}

                                    <a href="{{ route('movies.show', $item) }}" class="btn" style="background-color: #3498DB" title="Más información"><i class="fa fa-pencil" aria-hidden="true">Detalles</i>
                                    </a>     

                                    <a href="{{ route('movies.edit', $item) }}" class="btn" style="background-color: #FDC02E" title="Editar película"><i class="fa fa-pencil" aria-hidden="true">Editar</i>
                                    </a>    
                                     
                                    <button class="btn btn-danger" type="submit">Eliminar</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
</div>


</div>

@endsection

@section('js')
    {{-- JS DATATABLE --}}
    
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js" defer></script>
    
    <script>
        $(document).ready(function() {
            var table = $('#movies').DataTable({
                "search": {regex: true},
                "order": [[ 1, "asc" ]],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                }
            });

            $( "#dispo" ).click(function() {

                table
                .columns(3)
                .search($(this).text())
                .draw();

            });

            $( "#indispo" ).click(function() {

                table
                .columns(3)
                .search("Comprada|Alquilada", true, false)
                .draw();

            });

        });

    </script>
    @if (Session::has('created'))
        <script>
            toastr.success("Película creada.");
        </script>
    @endif
    @if (Session::has('deleted'))
        <script>
            toastr.error("Película eliminada.");
        </script>
    @endif
    @if (Session::has('edited'))
        <script>
            toastr.success("Película editada.");
        </script>
    @endif
@endsection
