@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reportes') }}</div>

                <div class="card-body">


                    <a href="{{ route('report.users.download-pdf') }}" class="btn" style="background-color: #FDC02E" title="Reporte"><i class="fa fa-pencil" aria-hidden="true">Descargar pdf usuarios</i>
                    </a>   

                    <a href="{{ route('report.users.download-excel') }}" class="btn" style="background-color: #FDC02E" title="Reporte"><i class="fa fa-pencil" aria-hidden="true">Descargar excel usuarios</i>
                    </a>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
